//
//  ClockHand.h
//  pic_clock
//
//  Created by Toida on 2015/06/16.
//
//

#ifndef __pic_clock__ClockHand__
#define __pic_clock__ClockHand__

#include <time.h>
#include "cocos2d.h"

USING_NS_CC;

class ClockHand : public cocos2d::Sprite
{
protected:
	
private:
	#define WIDTH_HOUR_HAND		8
	#define HEIGHT_HOUR_HAND	50
	#define WIDTH_MIN_HAND		4
	#define HEIGHT_MIN_HAND		90
	#define WIDTH_SEC_HAND		2
	#define HEIGHT_SEC_HAND		90

	void GetClockAngle(struct tm *t, float *hour_angle, float *min_angle, float *sec_angle);

	int m_hand;
public:
	#define PARAM_HOUR_HAND		0
	#define PARAM_MIN_HAND		1
	#define PARAM_SEC_HAND		2

	ClockHand();
	virtual ~ClockHand();
	static ClockHand* create(int hand);
	virtual bool init(int hand);

	void ClockHandUpdate();
};

#endif /* defined(__pic_clock__ClockHand__) */
