#ifndef __PIC_CLOCK_SCENE_H__
#define __PIC_CLOCK_SCENE_H__

#include <time.h>
#include "cocos2d.h"

class PicClock : public cocos2d::Layer
{
public:
	/* Z Order */
	enum{
		ZORDER_DIAL = 0
		,ZORDER_HOUR_HAND
		,ZORDER_MIN_HAND
		,ZORDER_SEC_HAND
		,ZORDER_CENTER
	};

	/* ITAG */
	enum{
		ITAG_DIAL = 0
		,ITAG_HOUR_HAND
		,ITAG_MIN_HAND
		,ITAG_SEC_HAND
		,ITAG_CENTER
	};

    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    
    // implement the "static create()" method manually
    CREATE_FUNC(PicClock);

	void update(float delta);
};

#endif // __PIC_CLOCK_SCENE_H__
